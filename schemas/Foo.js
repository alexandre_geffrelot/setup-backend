/* this is an example and can be deleted safely */

const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const Foo = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    city: {
      type: String,
    }
  },
  { timestamps: true }
);

module.exports = new mongoose.model("Foo", Foo);
