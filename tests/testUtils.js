/**
 * Common functions used in tests
 */

const { MongoMemoryServer } = require("mongodb-memory-server");
const mongoose = require("mongoose");
const { mongooseOpts } = require("../db");
const request = require("supertest");
const app = require("../app");
const { roles } = require("../globals");

module.exports = {
  /**
   * Spawn an in-memory Mongo database and returns it.
   * @returns {Promise<MongoMemoryServer>}
   */
  connectToTestDatabase: async () => {
    const mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, mongooseOpts, (err) => {
      if (err) console.error(err);
    });
    return mongoServer;
  },

  /**
   * Disconnects from the in-memory database and kills its process.
   * @param {MongoMemoryServer} mongoServer
   * @returns {Promise<void>}
   */
  disconnectFromTestDatabase: async (mongoServer) => {
    await mongoose.disconnect();
    await mongoServer.stop();
  },

  /**
   * @typedef {Object} TokenObject
   * @property {string} token - The JWT
   * @property {string} refreshToken - The refresh token
   */
  /**
   * Get a JWT and a refresh token for an account
   * @param {roles} role The role you want to have
   * @returns {Promise<TokenObject>} Resolves with a JWT and a refresh token
   */
  login: (role) => {
    return new Promise(async (resolve) => {
      let loginInfos;
      switch (role) {
        case roles.INVESTIGATOR:
          loginInfos = {
            email: "investigator@total.com",
            password: "xxii",
          };
          break;
        case roles.ADMIN:
          loginInfos = {
            email: "admin@gmail.com",
            password: "admin",
          };
          break;
        case roles.SUPER_ADMIN:
          loginInfos = {
            email: "super-admin@gmail.com",
            password: "xxii",
          };
          break;
        default:
          throw new Error("No role supplied for login.");
      }
      const response = await request(app).post("/login").send(loginInfos);
      resolve(response.body);
    });
  },
};
