const request = require("supertest");
const {
  connectToTestDatabase,
  disconnectFromTestDatabase,
  login,
} = require("../../testUtils");
const { roles } = require("../../../globals");
const app = require("../../../app");

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

let mongoServer = null;
let jwt = null;

// Connect to the test database, and insert common seeds that won't be modified by these tests
beforeAll(async () => {
  mongoServer = await connectToTestDatabase();
  jwt = (await login(roles.ADMIN)).token;

});

// Disconnect from the test database
afterAll(async () => {
  await disconnectFromTestDatabase(mongoServer);
});

// Seed the database with dummy data
beforeEach(async () => {
  /* Insert the seeds you will need in your case here */
  // ex: await User.insertMany(UserSeeds);

  /* ------------------------------------------------ */
});

// Clear database after each test
afterEach(async () => {
  /* Specify each imported seed here so it can be cleaned up */

  /* ------------------------------------------------ */
});

test("I can can read my name and city when i try to reach route /", async () => {
  const response = await request(app).get(
    `/?name=John&city=NYC`
  );
  expect(response.status).toBe(200);
  expect(response.body).toBe("hello John from NYC");
});

