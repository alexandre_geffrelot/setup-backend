module.exports = {
  /**
   * @enum {string}
   */
  roles: {
    SUPER_ADMIN: "super admin",
    ADMIN: "admin",
    USER: "user",
  },
  databaseUrl: process.env.MONGODB_URI,
  jwt: {
    secret: process.env.JWT_SECRET,
    sessionValidityDuration: 3600,
    refreshValidityDuration: "1d",
  },
};
