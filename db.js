const { testDatabaseUrl } = require("./globals");
const mongoose = require("mongoose");

/* ---- import your mongoose schemas here ---- */
// ex: const User = require("./schemas/User");
const Foo = require("./schemas/Foo");
/* ------------------------------------------------ */

/* ---- import your db seeds here ---- */
// ex: const userSeeds = require("./seeds/user");
const fooSeeds = require("./seeds/foo");
/* ------------------------------------------------ */

const mongooseOpts = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
};

const connect = (dbUrl) => {
  mongoose.connect(dbUrl, mongooseOpts).then(async () => {
    // We don't load the seeds in tests database
    if (dbUrl !== testDatabaseUrl) {
      // No data for companies ? We load the seeds

      /* For every schema you wish to insert seeds for insert your import here */
      // ex: const users = await User.find().estimatedDocumentCount();
      //       if (users === 0) {
      //         User.insertMany(userSeeds);
      //       }

      /* this is an example and can be deleted safely */
      const foos = await Foo.find().estimatedDocumentCount();
      if (foos === 0) {
        Foo.insertMany(fooSeeds);
      }
    }
    /* ------------------------------------------------- */

  });
};

const disconnect = () => {
  mongoose.disconnect();
};

module.exports = { connect, disconnect, mongooseOpts };
