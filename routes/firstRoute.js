const express = require("express");
const router = express.Router();
const asyncHandler = require("express-async-handler");
const createError = require("http-errors");

router.get(
  "/",
  asyncHandler(async (req, res, next) => {
    try {
      res.json("hello " + req.query.name + " from " + req.query.city);
    } catch (e) {
      next(createError(400, e));
    }
  })
);

module.exports = router;
