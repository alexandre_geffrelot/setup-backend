const createError = require("http-errors");
const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");

/* ---- insert your middleware here----  */
// ex: const auth = require("./middlewares/auth");

/* ------------------------------------ */

/* ---- Import you routes here ---- */
  // ex: const usersRouter = require("./routes/user");

/* this is an example and can be deleted safely */
const firstRouteRouter = require("./routes/firstRoute");
/* --------------------------- */
/* ------------------------------------ */

const app = express();

const corsOptions = {
  origin: process.env.FRONTEND_BASEURL,
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
};

app.use(cors(corsOptions));

app.use(logger(process.env.APP_ENV === "dev" ? "dev" : "short"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname + "/public"));

/* ---- Declare the name for your routes here ---- */
// ex: app.use("/login", loginRouter);

/* this is an example and can be deleted safely */
app.use("/", firstRouteRouter);
/* ----------------- */

/* ------------------------------------ */

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  const status = err.status || 500;
  if (status === 500) {
    console.error(res.body ? res.body : err);
  }
  res.status(status).json(err);
});

module.exports = app;
